import _ from 'lodash';

export function mancala(move, gameState) {
  var newBoard = gameState.board;
  var stonesToDistribute = gameState.board[move];
  var start = move + 1;
  var skip = gameState.player === 0 ? 13 : 6;
  var mancala = gameState.player === 0 ? 6 : 13;

  gameState.board[move] = 0;

  return distribute(start, stonesToDistribute, gameState, skip, mancala);
}

function distribute(start, stonesToDistribute, gameState, skip, mancala) {
  if (stonesToDistribute === 0) {
    if (oneSideIsEmpty(gameState.board)) {
      for (var index = 0; index < gameState.board.length; index++) {
        if (index < 6) {
          gameState.board[6] += gameState.board[index];
          gameState.board[index] = 0;
        } else if (index >= 7 && index < 13) {
          gameState.board[13] += gameState.board[index];
          gameState.board[index] = 0;
        }
      }
    }
    return gameState;
  } else if (start === skip) {
    return distribute(start + 1, stonesToDistribute, gameState, skip, mancala);
  } else if (start >= gameState.board.length) {
    return distribute(0, stonesToDistribute, gameState, skip, mancala);
  } else {
    gameState.board[start] = gameState.board[start] + 1;

    if (stonesToDistribute === 1) {
      if (start !== mancala) {
        gameState.player = (gameState.player + 1) % 2;
        if (gameState.board[start] === 1) {
          var opposite = 12 - start;
          if (gameState.board[opposite] > 0 ) {
            //capture
            gameState.board[start] = 0;
            gameState.board[mancala] += 1;
            var stolen_pieces = gameState.board[opposite];
            gameState.board[opposite] = 0;
            gameState.board[mancala] += stolen_pieces;
          }
        }
      }
    }

    return distribute(
      start + 1,
      stonesToDistribute - 1,
      gameState,
      skip,
      mancala
    );
  }
}

function oneSideIsEmpty(board) {
  var p1 = board.slice(0, 6);
  var p2 = board.slice(7, 13);
  var p1Sum = p1.reduce((a, b) => a + b, 0);
  var p2Sum = p2.reduce((a, b) => a + b, 0);
  return p1Sum == 0 || p2Sum == 0;
}
